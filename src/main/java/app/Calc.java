package app;

public class Calc {
    public static void main(String[] args) {
        int intValue = 5;
        squareOfIntValue(intValue);

        double radiusCylinder = 3.5;
        double heightCylinder = 10.0;
        System.out.println("Cylinder volume with radius " + radiusCylinder + " and height " + heightCylinder + " is equal " + calculateCylinderVolume(radiusCylinder, heightCylinder));

        int value = 2;
        int powValue = 3;
        System.out.println( value + " in the power " + powValue + " is equal " + calculateValueInPower(value, powValue));

    }

    /**
     * Напишіть метод, який приймає ціле число як аргумент і виводить його квадрат.
     * @param intValue (Введіть ціле число: 5)
     * (Виведе обчислення квадрату числа 5 дорівнює 25.)
     */
    public static void squareOfIntValue(int intValue) {
        int result = intValue * intValue;
        System.out.println("Square of " + intValue + " is equal " + result);
    }

    /**
     * Напишіть метод, який приймає два аргументи типу double - радіус та висоту - і повертає об'єм циліндра.
     * Виведіть об'єм циліндра на екран.
     * @param radiusCylinder (з радіусом 3.5)
     * @param heightCylinder (з висотою 10.0)
     * @return (Об'єм циліндра з радіусом 3.5 і висотою 10.0 дорівнює 384.81334713945307)
     */
    public static double calculateCylinderVolume (double radiusCylinder, double heightCylinder){
         return Math.PI*radiusCylinder*radiusCylinder*heightCylinder; // Метод тільки повертає результат обчислення без виводу на екран.
    }

    /**
     * Напишіть метод, який приймає два цілих числа, a та b, і повертає результат a^b (a підняте до степеня b).
     * @param value (Введіть a: 2)
     * @param powValue (Введіть b: 3)
     * @return (Результат 2^3 дорівнює 8)
     */
    public static int calculateValueInPower(int value, int powValue) {
        return (int)Math.pow(value,powValue); // Метод тільки повертає результат обчислення без виводу на екран.
    }
}
