package app;

import java.util.Scanner;

public class HomeTask05 {



    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);


        int age;
        do {
            System.out.println("ДЗ 5.1. Чи ти дорослий? \n Введіть ваш вік числом (примітка: Від'ємні числа не оброблюються і знову запитае на ваш вік");
            age = scan.nextInt();
        }
        while (age<0);
        System.out.println((isAdult(age))?"Ви доросла особа":"Ви не є дорослою особою");




        int potentialSquareNumber;
        do {
            System.out.println("ДЗ 5.2. Квадрат числа \n Введіть число для перевірки (примітка: Від'ємні числа не оброблюються і знову запитае число");
            potentialSquareNumber = scan.nextInt();
        }
        while (potentialSquareNumber<0);
        System.out.println("isPerfectSquare(" + potentialSquareNumber + ") → " + isPerfectSquare(potentialSquareNumber));




        int sequenceNumber;
        do {
            System.out.println("ДЗ 5.3. Числовий ряд  \n Введіть число N для підрахунку суми чисел в послідовності від 0 до N (примітка: Від'ємні числа не оброблюються і знову запитае число");
            sequenceNumber = scan.nextInt();
        }
        while (sequenceNumber<0);
        System.out.println((sequenceNumber==0)?"Хибні вхідні параметри":"calculateSumUpToN(" + sequenceNumber + ") → " + numbersAndTheirSumUpToN(sequenceNumber));




        int gradeScore;
        System.out.println("ДЗ 5.4. Оцінка студента   \n Введіть число що відповідає оцінці студента");
        gradeScore = scan.nextInt();
        System.out.println("getGrade(" + gradeScore + ") → " + getGrade(gradeScore));




        int simpleNumber;
        do {
            System.out.println("ДЗ 5.5. Просте число   \n Введіть число N для перевірки чи просте воно (примітка: Від'ємні числа не оброблюються і знову запитае число");
            simpleNumber = scan.nextInt();
        }
        while (simpleNumber<0);
        System.out.println("isPrime(" + simpleNumber + ") → " + isPrime(simpleNumber));



        }

    public static boolean isAdult (int age){
        return (age>=18);
    }

    public static boolean isPerfectSquare (int num) {
        return (((int) Math.sqrt(num) * (int) Math.sqrt(num)) == num);
    }

    public static String numbersAndTheirSumUpToN (int num) {
        int SumUpToN = 0;
        String result = "";
        for (int i = 1; i <= num; i++) {
            SumUpToN += i;
            result = (i==num)?result + i +  " = " + SumUpToN:result + i +  " + ";
        }
        return result;
    }


    public static String getGrade(int grade) {
        switch (grade) {
            case 1 -> {return "Погано";}
            case 2 -> {return "Незадовільно";}
            case 3 -> {return "Задовільно";}
            case 4 -> {return "Добре";}
            case 5 -> {return "Відмінно";}
            default -> {return "Неправильна оцінка";}
        }
    }


        public static boolean isPrime(int number) {
            for (int i = 2; i <= Math.sqrt(number); i++) {
                if ((number <= 1)||(number % i == 0)) {
                    return false;
                }
            }
            return true;
        }

}