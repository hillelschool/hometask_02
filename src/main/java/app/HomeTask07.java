package app;

import java.util.Scanner;

public class HomeTask07 {

    public static void main(String[] args) {

        int[] arrayIntNumbers = {5, 4, 3, 2, 1, 6, 7, 8, 9, 10, 14, 15};
        System.out.println("Average value : " + averageValue(arrayIntNumbers));
        System.out.println("Minimal value : " + calculateMinValue(arrayIntNumbers));
        System.out.println("Maximum value : " + calculateMaxValue(arrayIntNumbers));
        printReverseArray(arrayIntNumbers);

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a value for searching in the array: ");
        int searchValue = scan.nextInt();

        boolean isFound = checkValueInArray(arrayIntNumbers, searchValue);
        System.out.println("The value " + searchValue + " is" + ((isFound)?" ":" not ") + "present in the array.");

    }

    public static double averageValue(int[] array) {
        int sum = 0;
        for (int num : array) {
            sum += num;
        }
        return (array.length == 0)?0.0:(double)(sum / array.length);
    }

    public static int calculateMinValue(int[] array) {
        int min = array[0];
        for (int i = 1; i < array.length; i++) {
            min = (min > array[i])?array[i]:min;
        }
        return min;
    }

    public static int calculateMaxValue(int[] array) {
        int max = array[0];
        for (int i = 1; i < array.length; i++) {
            max = (max < array[i])?array[i]:max;
        }
        return max;
    }

    public static void printReverseArray(int[] array) {
        if (array == null || array.length == 0) {
            System.out.println("Array is empty or null");
        }
        else {
            System.out.print("Array in reverse order: ");
            for (int i = array.length - 1; i >= 0; i--) {
                System.out.print(array[i] + " ");
            }
            System.out.println();
        }
    }

    public static boolean checkValueInArray(int[] array, int value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                return true;
            }
        }
        return false;
    }

}
