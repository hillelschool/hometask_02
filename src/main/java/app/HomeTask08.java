package app;

public class HomeTask08 {
    public static void main(String[] args) {
        System.out.println(catDog("catdog"));
        System.out.println(catDog("catcat"));
        System.out.println(catDog("1cat1cadodog"));

        int[] arr1 = {2, 1, 2, 3, 4};
        int[] arr2 = {2, 2, 0};
        int[] arr3 = {1, 3, 5};

        System.out.println(countEvenInts(arr1));
        System.out.println(countEvenInts(arr2));
        System.out.println(countEvenInts(arr3));


        int[] arr4 = {1, 2, 3, 4, 100};
        int[] arr5 = {1, 1, 5, 5, 10, 8, 7};
        int[] arr6 = {-10, -4, -2, -4, -2, 0};

        System.out.println(centeredAverage(arr4));
        System.out.println(centeredAverage(arr5));
        System.out.println(centeredAverage(arr6));

        int[] arr7 = {1, 2, 2};
        int[] arr8 = {1, 2, 2, 6, 99, 99, 7};
        int[] arr9 = {1, 1, 6, 7, 2};

        System.out.println(sumIgnoreSections(arr7));
        System.out.println(sumIgnoreSections(arr8));
        System.out.println(sumIgnoreSections(arr9));

        int[] a13rr1 = {1, 2, 2,1};
        int[] a13rr2 = {1, 1};
        int[] a13rr3 = {1, 2, 2, 1, 13};

        System.out.println(sumWithoutUnlucky13(a13rr1));
        System.out.println(sumWithoutUnlucky13(a13rr2));
        System.out.println(sumWithoutUnlucky13(a13rr3));

        int[] arr10 = {10, 3, 5, 6};
        int[] arr11 = {7, 2, 10, 9};
        int[] arr12 = {2, 10, 7, 2};

        System.out.println(differenceLargestSmallest(arr10));
        System.out.println(differenceLargestSmallest(arr11));
        System.out.println(differenceLargestSmallest(arr12));

        String result1 = doubleChars("The");
        String result2 = doubleChars("AAbb");
        String result3 = doubleChars("Hi-There");

        System.out.println(result1);
        System.out.println(result2);
        System.out.println(result3);


        System.out.println(countHi("abc hi ho"));
        System.out.println(countHi("ABChi hi"));
        System.out.println(countHi("hihi"));


        System.out.println(countCode("aaacodebbb"));
        System.out.println(countCode("codexxcode"));
        System.out.println(countCode("cozexxcope"));

        System.out.println(endsWith("AbC", "HiaBc"));
        System.out.println(endsWith("abc", "abXabc"));
        System.out.println(endsWith("Hiabc", "abc"));
    }


    public static boolean catDog(String str) {
        int catCount = 0;
        int dogCount = 0;
        for (int i = 0; i < str.length() - 2; i++) {
            if (str.substring(i, i + 3).equals("cat")) {
                catCount++;
            } else if (str.substring(i, i + 3).equals("dog")) {
                dogCount++;
            }
        }
        return catCount == dogCount;

    }

    public static int countEvenInts(int[] nums) {
        int count = 0;
        for (int num : nums) {
            count = (num % 2 == 0)?count+1:count;
        }
        return count;
    }

    public static int centeredAverage(int[] nums) {
        int min = nums[0];
        int max = nums[0];
        for (int num : nums) {
            if (num < min) {
                min = num;
            }
            if (num > max) {
                max = num;
            }
        }
        int sum = 0;
        int count = 0;
        boolean minFlagOnce = false;
        boolean maxFlagOnce = false;
        for (int num : nums) {
            if ((minFlagOnce == false) && (num == min)) {
                minFlagOnce = true;
                continue;
            }
            if ((maxFlagOnce == false) && (num == max)) {
                maxFlagOnce = true;
                continue;
            }
            if ((minFlagOnce == true) && (num == min)) {
                sum += num;
                count++;
                continue;
            }
            else if ((maxFlagOnce == true) && (num == max)) {
                sum += num;
                count++;
                continue;
            }
                if (num != min && num != max) {
                    sum += num;
                    count++;
                }
        }
        return sum / count;
    }

    public static int sumIgnoreSections(int[] nums) {
        int sum = 0;
        boolean ignoring = false;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 6) {
                ignoring = true;
            } else if (nums[i] == 7 && ignoring) {
                ignoring = false;
            } else if (!ignoring) {
                sum += nums[i];
            }
        }

        return sum;
    }

    public static int sumWithoutUnlucky13(int[] nums) {
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 13) {
                sum += nums[i];
            }
        }
        return sum;
    }



    public static int differenceLargestSmallest(int[] nums) {
        int min = nums[0];
        int max = nums[0];
        for (int num : nums) {
            if (num < min) {
                min = num;
            }
            if (num > max) {
                max = num;
            }
        }
        int difference = max - min;

        return difference;
    }

    public static String doubleChars(String str) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            result.append(ch).append(ch);
        }
        return result.toString();
    }

    public static int countHi(String str) {
        int count = 0;
        int index = 0;
        while (index < str.length() - 1) {
            if (str.substring(index, index + 2).equals("hi")) {
                count++;
            }
            index++;
        }

        return count;
    }

    public static int countCode(String str) {
        int count = 0;
        for (int i = 0; i < str.length() - 3; i++) {
            if (str.substring(i, i + 2).equals("co") &&
                    str.charAt(i + 3) == 'e') {
                count++;
            }
        }

        return count;
    }


    public static boolean endsWith(String str1, String str2) {
        str1 = str1.toLowerCase();
        str2 = str2.toLowerCase();
        return str1.endsWith(str2)||str2.endsWith(str1);
    }

}
