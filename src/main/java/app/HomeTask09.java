package app;

import java.util.*;

public class HomeTask09 {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(3);
        numbers.add(14);
        numbers.add(15);
        numbers.add(92);
        numbers.add(6);

        System.out.println("Елементи колекції");
        for (int number : numbers) {
            System.out.println(number);

        }

        List<String> strings = new ArrayList<>();
        strings.add("Роман");
        strings.add("Боженко");
        strings.add("Вікторович");

        System.out.println("ПІБ");

        for (String string : strings) {
            System.out.println(string);
        }

        HashSet<String> cities = new HashSet<String>();
        cities.add("Kyiv");
        cities.add("Dnipro");
        cities.add("Lviv");

        cities.add("Lviv"); //Виводить лише один раз в консолі

        System.out.println("\nCities : ");
        for (String city : cities) {
            System.out.println(city);
        }

        Map<String, String> citiesAndCodes = new HashMap<>();

        citiesAndCodes.put("Київ", "044");
        citiesAndCodes.put("Харків", "057");
        citiesAndCodes.put("Львів", "032");
        citiesAndCodes.put("Одеса", "048");
        citiesAndCodes.put("Дніпро", "056");
        citiesAndCodes.put("Запоріхжжя", "061");

        System.out.println("\nМапа міст і їх телефонних кодів:");
        for (Map.Entry<String, String> entry : citiesAndCodes.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }

    }

}
